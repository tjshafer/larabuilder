<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaffRolesTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('staff_roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 50);
            $table->text('description')->nullable();
            $table->bigInteger('company_id');
            $table->timestamps();
        });

        Schema::table('permissions', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->bigInteger('role_id')->after('id');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->bigInteger('role_id')->after('user_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('staff_roles');
        Schema::table('permissions', function (Blueprint $table) {
            $table->dropColumn(['role_id']);
            $table->bigInteger('user_id')->after('id');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['role_id']);
        });
    }
}
