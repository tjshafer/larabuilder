<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelatedToCompanyEmailTemplate extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('company_email_template', function (Blueprint $table) {
            $table->string('related_to')->after('body')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('company_email_template', function (Blueprint $table) {
            $table->dropColumn(['related_to']);
        });
    }
}
