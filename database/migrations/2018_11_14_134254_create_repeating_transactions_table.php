<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepeatingTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('repeating_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('trans_date');
            $table->bigInteger('account_id');
            $table->bigInteger('chart_id');
            $table->string('type', 10);
            $table->string('dr_cr', 2);
            $table->decimal('amount', 10, 2);
            $table->decimal('base_amount', 10, 2)->nullable();
            $table->bigInteger('payer_payee_id')->nullable();
            $table->bigInteger('payment_method_id');
            $table->string('reference', 50)->nullable();
            $table->text('note')->nullable();
            $table->bigInteger('company_id');
            $table->tinyInteger('status')->nullable()->default(0);
            $table->bigInteger('trans_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('repeating_transactions');
    }
}
