<?php

namespace App\Http\Middleware;

use Closure;

class CanInstall
{
    /**
     * Handle an incoming request.
     *
     * @return mixed
     */
    public function handle(\Illuminate\Http\Request $request, Closure $next)
    {
        if (env('APP_INSTALLED', true) == true) {
            update_currency_exchange_rate();

            return $next($request);
        }

        return redirect('/installation');
    }
}
