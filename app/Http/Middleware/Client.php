<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Http\Response;

class Client
{
    /**
     * Handle an incoming request.
     *
     * @return mixed
     */
    public function handle(\Illuminate\Http\Request $request, Closure $next)
    {
        if (Auth::User()->user_type == 'client') {
            return $next($request);
        }
        if (! $request->ajax()) {
            return back()->with('error', _lang('Permission denied !'));
        }

        return new Response('<h5 class="text-center red">'._lang('Permission denied !').'</h5>');

        return $next($request);
    }
}
