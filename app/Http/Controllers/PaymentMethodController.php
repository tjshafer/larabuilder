<?php

namespace App\Http\Controllers;

use App\PaymentMethod;
use Illuminate\Http\Request;
use Validator;

class PaymentMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $paymentmethods = PaymentMethod::where('company_id', company_id())
                                       ->orderBy('id', 'desc')->get();

        return view('backend.accounting.payment_method.list', compact('paymentmethods'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (! $request->ajax()) {
            return view('backend.accounting.payment_method.create');
        }

        return view('backend.accounting.payment_method.modal.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50',
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json(['result' => 'error', 'message' => $validator->errors()->all()]);
            }

            return redirect('payment_methods/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $paymentmethod = new PaymentMethod();
        $paymentmethod->name = $request->input('name');
        $paymentmethod->company_id = company_id();

        $paymentmethod->save();

        if (! $request->ajax()) {
            return redirect('payment_methods/create')->with('success', _lang('Saved Sucessfully'));
        }

        return response()->json(['result' => 'success', 'action' => 'store', 'message' => _lang('Saved Sucessfully'), 'data' => $paymentmethod]);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, int $id)
    {
        $paymentmethod = PaymentMethod::where('id', $id)
                                        ->where('company_id', company_id())->first();
        if (! $request->ajax()) {
            return view('backend.accounting.payment_method.view', compact('paymentmethod', 'id'));
        }

        return view('backend.accounting.payment_method.modal.view', compact('paymentmethod', 'id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, int $id)
    {
        $paymentmethod = PaymentMethod::where('id', $id)
                                      ->where('company_id', company_id())->first();
        if (! $request->ajax()) {
            return view('backend.accounting.payment_method.edit', compact('paymentmethod', 'id'));
        }

        return view('backend.accounting.payment_method.modal.edit', compact('paymentmethod', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50',
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json(['result' => 'error', 'message' => $validator->errors()->all()]);
            }

            return redirect()->route('payment_methods.edit', $id)
                        ->withErrors($validator)
                        ->withInput();
        }

        $paymentmethod = PaymentMethod::where('id', $id)
                                        ->where('company_id', company_id())->first();
        $paymentmethod->name = $request->input('name');
        $paymentmethod->company_id = company_id();

        $paymentmethod->save();

        if (! $request->ajax()) {
            return redirect('payment_methods')->with('success', _lang('Updated Sucessfully'));
        }

        return response()->json(['result' => 'success', 'action' => 'update', 'message' => _lang('Updated Sucessfully'), 'data' => $paymentmethod]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id): \Illuminate\Http\RedirectResponse
    {
        $paymentmethod = PaymentMethod::where('id', $id)
                                        ->where('company_id', company_id());
        $paymentmethod->delete();

        return redirect('payment_methods')->with('success', _lang('Removed Sucessfully'));
    }
}
