<?php

namespace App\Http\Controllers;

use App\EmailSubscriber;

class EmailSubscriberController extends Controller
{
    public function __construct()
    {
        date_default_timezone_set(get_option('timezone', 'Asia/Dhaka'));
    }

    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $email_subscribers = EmailSubscriber::orderBy('id', 'desc')
                                            ->get();

        return view('backend.email_subscriber.list', compact('email_subscribers'));
    }
}
