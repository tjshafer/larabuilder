<?php

namespace App\Http\Controllers;

use App\CompanyEmailTemplate;
use Illuminate\Http\Request;
use Validator;

class CompanyEmailTemplateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
    {
        $companyemailtemplates = CompanyEmailTemplate::where('company_id', company_id())
                                                     ->orderBy('id', 'desc')->get();

        return view('backend.accounting.company_email_template.list', compact('companyemailtemplates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (! $request->ajax()) {
            return view('backend.accounting.company_email_template.create');
        }

        return view('backend.accounting.company_email_template.modal.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'related_to' => 'required',
            'name' => 'required|max:191',
            'subject' => 'required|max:191',
            'body' => 'required',
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json(['result' => 'error', 'message' => $validator->errors()->all()]);
            }

            return redirect('company_email_template/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $companyemailtemplate = new CompanyEmailTemplate();
        $companyemailtemplate->related_to = $request->input('related_to');
        $companyemailtemplate->name = $request->input('name');
        $companyemailtemplate->subject = $request->input('subject');
        $companyemailtemplate->body = $request->input('body');
        $companyemailtemplate->company_id = company_id();

        $companyemailtemplate->save();

        if (! $request->ajax()) {
            return redirect('company_email_template/create')->with('success', _lang('Saved Sucessfully'));
        }

        return response()->json(['result' => 'success', 'action' => 'store', 'message' => _lang('Saved Sucessfully'), 'data' => $companyemailtemplate]);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, int $id)
    {
        $companyemailtemplate = CompanyEmailTemplate::where('id', $id)
                                                    ->where('company_id', company_id())->first();
        if (! $request->ajax()) {
            return view('backend.accounting.company_email_template.view', compact('companyemailtemplate', 'id'));
        }

        return view('backend.accounting.company_email_template.modal.view', compact('companyemailtemplate', 'id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, int $id)
    {
        $companyemailtemplate = CompanyEmailTemplate::where('id', $id)
                                                    ->where('company_id', company_id())->first();
        if (! $request->ajax()) {
            return view('backend.accounting.company_email_template.edit', compact('companyemailtemplate', 'id'));
        }

        return view('backend.accounting.company_email_template.modal.edit', compact('companyemailtemplate', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $validator = Validator::make($request->all(), [
            'related_to' => 'required',
            'name' => 'required|max:191',
            'subject' => 'required|max:191',
            'body' => 'required',
        ]);

        if ($validator->fails()) {
            if ($request->ajax()) {
                return response()->json(['result' => 'error', 'message' => $validator->errors()->all()]);
            }

            return redirect()->route('company_email_template.edit', $id)
                        ->withErrors($validator)
                        ->withInput();
        }

        $companyemailtemplate = CompanyEmailTemplate::where('id', $id)->where('company_id', company_id())->first();
        $companyemailtemplate->related_to = $request->input('related_to');
        $companyemailtemplate->name = $request->input('name');
        $companyemailtemplate->subject = $request->input('subject');
        $companyemailtemplate->body = $request->input('body');
        $companyemailtemplate->company_id = company_id();

        $companyemailtemplate->save();

        if (! $request->ajax()) {
            return redirect('company_email_template')->with('success', _lang('Updated Sucessfully'));
        }

        return response()->json(['result' => 'success', 'action' => 'update', 'message' => _lang('Updated Sucessfully'), 'data' => $companyemailtemplate]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id): \Illuminate\Http\RedirectResponse
    {
        $companyemailtemplate = CompanyEmailTemplate::where('id', $id)
                                                    ->where('company_id', company_id());
        $companyemailtemplate->delete();

        return redirect('company_email_template')->with('success', _lang('Deleted Sucessfully'));
    }

    public function get_template($id): void
    {
        $companyemailtemplate = CompanyEmailTemplate::where('id', $id)
                                                    ->where('company_id', company_id())->first();
        echo json_encode($companyemailtemplate);
    }
}
