<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Services\SocialGoogleAccountService;
use App\Utilities\Overrider;
use Socialite;

class SocialAuthGoogleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        Overrider::load('ServiceSettings');
    }

    /**
     * Create a redirect method to google api.
     *
     * @return void
     */
    public function redirect()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Return a callback method from google api.
     *
     * @return callable URL from google
     */
    public function callback(SocialGoogleAccountService $service): \Illuminate\Http\RedirectResponse
    {
        $user = $service->createOrGetUser(Socialite::driver('google')->user());
        if ($user != null) {
            auth()->login($user);

            return redirect()->to('/dashboard');
        }

        return redirect()->to('/login')->with('error', _lang('Sorry, We did not find any account associated with your email !'));
    }
}
