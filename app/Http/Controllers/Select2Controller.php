<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class Select2Controller extends Controller
{
    public function __construct()
    {
        date_default_timezone_set(get_company_option('timezone', get_option('timezone', 'Asia/Dhaka')));
    }

    /**
     * Display a listing of the resource.
     */
    public function get_table_data(Request $request): \Illuminate\Http\Response
    {
        $data_where = [
            '1' => ['company_id' => company_id()], //general company Data
            '2' => ['company_id' => company_id(), 'item_type' => 'product'], //Item Type Product
            '3' => ['company_id' => company_id(), 'type' => 'income'], //Income Category
            '4' => ['company_id' => company_id(), 'type' => 'expense'], //Expense Category
            '5' => ['company_id' => company_id(), 'item_type' => 'service'], //Item Type Service
        ];

        $table = $request->get('table');
        $value = $request->get('value');
        $display = $request->get('display');
        $display2 = $request->get('display2');
        $where = $request->get('where');

        $q = $request->get('q');

        $display_option = "$display as text";
        if ($display2 != '') {
            $display_option = "CONCAT($display,' - ',$display2) AS text";
        }

        if ($where != '') {
            return DB::table($table)
                          ->select("$value as id", DB::raw($display_option))
                          ->where($display, 'LIKE', "$q%")
                          ->where($data_where[$where])
                          ->get();
        }

        return DB::table($table)
                      ->select("$value as id", DB::raw($display_option))
                      ->where($display, 'LIKE', "$q%")
                      ->get();
    }
}
