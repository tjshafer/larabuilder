<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'projects';

    public function members(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(\App\User::class, 'project_members', 'project_id', 'user_id');
    }

    public function setCustomDomainAttribute($value): void
    {
        if (empty($value)) {
            $this->attributes['custom_domain'] = null;
        } else {
            $this->attributes['custom_domain'] = $value;
        }
    }

     public function setSubDomainAttribute($value): void
     {
         if (empty($value)) {
             $this->attributes['sub_domain'] = null;
         } else {
             $this->attributes['sub_domain'] = $value;
         }
     }
}
