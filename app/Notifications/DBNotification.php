<?php

namespace App\Notifications;

use Illuminate\Notifications\DatabaseNotification;

class DBNotification extends DatabaseNotification
{
    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\User::class, 'user_id')->withDefault();
    }
}
