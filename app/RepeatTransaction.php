<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RepeatTransaction extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'repeating_transactions';

    public function account(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Account')->withDefault();
    }

    public function income_type(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\ChartOfAccount', 'chart_id')->withDefault();
    }

    public function payer(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Contact', 'payer_payee_id')->withDefault();
    }

    public function expense_type(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\ChartOfAccount', 'chart_id')->withDefault();
    }

    public function payee(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Contact', 'payer_payee_id')->withDefault();
    }

    public function payment_method(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\PaymentMethod::class, 'payment_method_id')->withDefault();
    }
}
