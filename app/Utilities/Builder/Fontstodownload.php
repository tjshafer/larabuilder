<?php

namespace App\Utilities\Builder;

class FontsToDownload extends Fonts
{
    private array $fontsNames = [];

    public function __construct($style, private $zip)
    {
        $this->basePath = SUPRA_BASE_PATH;
        $this->matchFonts($style);
    }

    public function matchFonts($style): void
    {
        preg_match_all('/font-family:\s?\'?([^,;\']*)\'?[,;]/', $style, $maches);
        $this->fontsNames = array_merge($this->fontsNames, array_unique($maches[1]));
    }

    public function getIncludeFonts(): void
    {
        $this->_searchFontsFiles($this->basePath.'/fonts');

        $array = [];
        foreach ($this->files as $key => $value) {
            foreach ($this->fontsNames as $name) {
                if (str_contains($key, $name.'-')) {
                    $array[$key] = $value;
                    $this->zip->addFile($value['path'].'/'.$value['fonts'][0], 'fonts/'.$value['fonts'][0]);
                }
            }
        }

        $this->files = $array;

        $fontCSSContent = $this->getFontCSSContent();
        $font_css = $fontCSSContent['css'];
        $font_css = preg_replace('#('.$this->basePath.')?(/[\w/_()-]*/)#im', '/fonts/', $font_css);

        $this->zip->addFromString(
            'css/fonts.css', $font_css
        );
    }
}
