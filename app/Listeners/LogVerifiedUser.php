<?php

namespace App\Listeners;

use App\EmailTemplate;
use App\Mail\RegistrationMail;
use App\Utilities\Overrider;
use Illuminate\Auth\Events\Verified;
use Illuminate\Support\Facades\Mail;

class LogVerifiedUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(Verified $event): void
    {
        $user = $event->user;
        //Replace paremeter
        $replace = [
            '{name}' => $user->name,
            '{email}' => $user->email,
            '{valid_to}' => date('d M,Y', strtotime($user->valid_to)),
        ];

        //Send Welcome email
        Overrider::load('Settings');
        $template = EmailTemplate::where('name', 'registration')->first();
        $template->body = process_string($replace, $template->body);

        try {
            Mail::to($user->email)->send(new RegistrationMail($template));
        } catch (\Exception $e) {
            // Nothing
        }
    }
}
