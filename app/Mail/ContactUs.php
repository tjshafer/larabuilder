<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactUs extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct(
        /**
         * Create a new message instance.
         *
         * @return void
         */
        protected $content
    ) {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): static
    {
        return $this->subject($this->content->subject)
                    ->markdown('backend.email.contact')
                    ->with('content', $this->content);
    }
}
