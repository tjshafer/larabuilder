<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InvoiceReceiptMail extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct(
        /**
         * Create a new message instance.
         *
         * @return void
         */
        protected $content
    ) {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): static
    {
        return $this->subject($this->content->subject)
                    ->markdown('backend.email.invoice_payment_receipt')
                    ->with('content', $this->content);
    }
}
